import torch.utils.data as data
import csv
from PIL import Image
import os.path
import numpy as np
import random as rand
from skimage.measure import compare_ssim as ssim

from utils import Cholec80, DataPrep


def _load_img(path, width, height):
    im = Image.open(path)
    w = im.width
    h = im.height
    im_scaled = im.resize((width, int(width * (h / w))))  # preserve aspect ratio

    img = Image.new('RGB', (width, height), (0, 0, 0))
    offset_y = (height - im_scaled.height) // 2
    img.paste(im_scaled, box=(0, offset_y))
    return img


class PhaseData(data.Dataset):
    """Dataset consisting of the sequential frames and their corresponding phase labels of one specific Cholec80 video.

    Frames will be sampled at 1 Hz.
    """

    def __init__(self, frames_path, annotation_path, width, height, transform=None):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param annotation_path: Path to the text file that contains the phase labels.
        :param width: Width of each frame in the dataset.
        :param height: Height of each frame in the dataset.
        :param transform: Image transformation applied to each frame in the dataset.
        """
        self.frames_path = frames_path
        self.width = width
        self.height = height
        self.transform = transform

        self.targets = []
        f = open(annotation_path, "r")
        reader = csv.reader(f, delimiter='\t')
        next(reader, None)
        count = 0
        for row in reader:
            if count % 25 == 0:  # read annotations with 1 fps
                self.targets.append(Cholec80.phase_map[row[1]])
            count += 1
        f.close()

    def __getitem__(self, index):
        """Return the frame at the given index along with its phase label.

        :param index: The number of the frame to return.
        :return: The tuple (frame, label).
        """

        target = self.targets[index]
        frame = self.frames_path + "/%08d.png" % (DataPrep.sample_rate * index)  # read frames with 1 fps
        img = _load_img(frame, self.width, self.height)

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        """ Return size of dataset. """

        return len(self.targets)


def _calc_ssim(img1, img2):
    img1_gray = img1.convert('L')
    img2_gray = img2.convert('L')
    return ssim(np.asarray(img1_gray), np.asarray(img2_gray))


class TupleData(data.Dataset):
    """ Dataset consisting of randomly sampled video frame tuples from one specific Cholec80 video."""

    def __init__(self, frames_path, sample_count=250, width=384, height=216, transform=None, delta=30.0, gamma=120.0, plus_2nd_order_TC=False):
        """Create dataset.

        :param frames_path: Path to the directory that contains the video frames.
        :param sample_count: Number of tuples in the dataset.
        :param width: Width of each frame (in each tuple) in the dataset.
        :param height: Height of each frame (in each tuple) in the dataset.
        :param transform: Image transformation applied to each frame (in each tuple) in the dataset.
        :param delta: Parameter to determine the size of the window from which a close frame is sampled.
                      Close frames are at most "delta" seconds away from a reference frame.
        :param gamma: Parameter to determine from where a distant frame is sampled.
                      Distant frames are at least "gamma" seconds away from a reference frame.
        :param plus_2nd_order_TC: Whether or not a tuple contains two close frames as required for training with
                                  2nd order temporal coherency.
        """
        self.frames_path = frames_path
        self.sample_count = sample_count
        self.delta = int(delta // (1/DataPrep.sample_rate))  # convert to frame-level distance
        self.gamma = int(gamma // (1/DataPrep.sample_rate))  # convert to frame-level distance
        self.plus_2nd_order_TC = plus_2nd_order_TC
        self.transform = transform
        self.width = width
        self.height = height

        self.frames = []
        for file in list(np.unique(os.listdir(frames_path))):
            if file.endswith('.png'):
                frame_path = os.path.join(frames_path, file)
                self.frames.append(frame_path)

    def __load_img(self, path):
        return _load_img(path, self.width, self.height)

    def __getitem__(self, index):
        """Return next tuple.

        Each tuple consists of a reference frame, one or two frames that are close to the reference, and a frame that is
        far from the reference.
        :param index: Will be ignored since every tuple is sampled randomly.
        :return: The tuple (reference frame, close frame, far frame) if "plus_2nd_order_TC" has been set to False,
                 otherwise (reference frame, close frame_1, close_frame_2, far frame)
        """
        # sample reference image
        seed = rand.randint(0, len(self.frames) - 1)
        img = self.__load_img(self.frames[seed])

        # sample close image(s)
        idx_c = rand.choice(list(i for i in list(range(max(0, seed - self.delta),
                                                       min(seed + self.delta + 1, len(self.frames)))) if i != seed))
        img_c2 = None
        if self.plus_2nd_order_TC:
            my_delta = idx_c - seed
            # handle corner case (idx_c2 out of range)
            if (my_delta < 0 and (seed + 2 * my_delta) < 0) or (my_delta > 0 and (seed + 2 * my_delta >= len(self.frames))):
                my_delta = my_delta * -1
                idx_c = seed + my_delta
            idx_c2 = idx_c + my_delta
            img_c2 = self.__load_img(self.frames[idx_c2])
        img_c = self.__load_img(self.frames[idx_c])

        # sample far image
        img_f = None
        success = False
        while not success:
            idx_f = rand.choice(list(range(0, max(0, seed - self.gamma + 1)))
                                  + list(range(min(seed + self.gamma, len(self.frames)), len(self.frames))))
            img_f = self.__load_img(self.frames[idx_f])
            s = _calc_ssim(img, img_f)
            if s > 0.7:
                img_f = None    # discard far images that are too similar to the seed image
            else:
                success = True

        result = []
        if self.plus_2nd_order_TC:
            result = [img, img_c, img_c2, img_f]
        else:
            result = [img, img_c, img_f]

        if self.transform is not None:
            result = map(self.transform, result)

        return tuple(result)

    def __len__(self):
        """ Return size of dataset. """

        return self.sample_count
