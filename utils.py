from __future__ import print_function
import os.path
import torchvision.transforms as transforms
import time
from sys import stderr


class Cholec80:
    """Define parameters related to the Cholec80 dataset."""

    num_phases = 7
    """Number of surgical phases."""

    phase_map = dict()
    phase_map['Preparation'] = 0
    phase_map['CalotTriangleDissection'] = 1
    phase_map['ClippingCutting'] = 2
    phase_map['GallbladderDissection'] = 3
    phase_map['GallbladderPackaging'] = 4
    phase_map['CleaningCoagulation'] = 5
    phase_map['GallbladderRetraction'] = 6

    op_sets = ['A', 'B', 'C', 'D']
    op_set_size = 20
    videos_in_set = [['02','04','06','12','24','29','34','37','38','39','44','58','60','61','64','66','75','78','79','80'],
                    ['01','03','05','09','13','16','18','21','22','25','31','36','45','46','48','50','62','71','72','73'],
                    ['10','15','17','20','32','41','42','43','47','49','51','52','53','55','56','69','70','74','76','77'],
                    ['07','08','11','14','19','23','26','27','28','30','33','35','40','54','57','59','63','65','67','68']]
    train_sets = op_sets[:3]
    test_sets = op_sets[3:]


class DataPrep:
    """Define parameters related to data preparation."""

    sample_rate = 5
    """Sample rate in Hz (fps) used for video frame extraction."""

    width = 384
    """Width in pixels of each extracted video frame."""

    height = 216
    """Height in pixels of each extracted video frame."""

    standard_transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    """Image transformation applied to each extracted video frame."""


def log(file, msg):
    """Log a message.

    :param file: File object to which the message will be written.
    :param msg:  Message to log (str).
    """
    print(time.strftime("[%d.%m.%Y %H:%M:%S]: "), msg, file=stderr)
    file.write(time.strftime("[%d.%m.%Y %H:%M:%S]: ") + msg + os.linesep)


data_path = "---"
"""Path to the directory that contains the prepared Cholec80 data."""

out_path = "./out"
"""Path to the directory where experimental results are stored."""

videos_path = os.path.join(data_path, "videos")
frames_path = os.path.join(data_path, "frames")
annotation_path = os.path.join(data_path, "phase_annotations")
