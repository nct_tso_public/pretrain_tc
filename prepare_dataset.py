"""Script to prepare the Cholec80 data for experiments.

Requires the "data_path" variable in utils.py to be set to the directory that contains the Cholec80 data.
Will create the subdirectory "frames" in "data_path".
For each Cholec80 video, frames will be extracted at 5 Hz and will be saved to data_path/frames/<op_set>/<op_id>,
where <op_set> is the set to which the video belongs (either A, B, C, or D) and <op_id> is the number of the video.
"""

import os
import cv2
import imageio
import utils

resolution = [utils.DataPrep.width, utils.DataPrep.height]  # Resolution for output


def computeVideoImages(videoPath, outputPath):
    os.makedirs(outputPath, exist_ok=True)
    vid = imageio.get_reader(videoPath)  # Read video
    fps = vid.get_meta_data()['fps']
    startFrame = 0
    endFrame = vid.get_length()
    sample_rate = int(fps)//utils.DataPrep.sample_rate
    count = 0
    for frame in range(startFrame, endFrame, sample_rate):
        try:
            image = vid.get_data(frame)
        except imageio.core.format.CannotReadFrameError:
            print("Couldn't read frame %d of %d" % (frame, endFrame))
            break

        # assume wanted ratio <= actual ratio of the image
        destWidth = image.shape[0] * (resolution[0] / resolution[1])
        
        image = image[:, int(image.shape[1] / 2 - destWidth / 2):int(image.shape[1] / 2 + destWidth / 2)]  # center crop
        image = cv2.resize(image, (resolution[0], resolution[1]))  # resize
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        name = str(count).zfill(8) + ".png"
        cv2.imwrite(os.path.join(outputPath, name), image)  # save frame as png file

        count += 1


if __name__ == "__main__":
    frames_path = utils.frames_path
    videos_path = utils.videos_path
    for i in range(len(utils.Cholec80.op_sets)):
        for j in range(len(utils.Cholec80.videos_in_set[i])):
            print(utils.Cholec80.op_sets[i] + " " + utils.Cholec80.videos_in_set[i][j])
            videoPath = os.path.join(videos_path, "video" + utils.Cholec80.videos_in_set[i][j] + ".mp4")
            outputPath = os.path.join(frames_path, utils.Cholec80.op_sets[i], utils.Cholec80.videos_in_set[i][j])

            computeVideoImages(videoPath, outputPath)
