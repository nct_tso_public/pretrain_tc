import torch
import torch.nn as nn
import torchvision.models.resnet

from utils import Cholec80


class FeatureNet(nn.Module):
    """FeatureNet."""

    num_features = 4096
    """Number of output neurons, i.e., dimension of the learned feature space."""

    def __init__(self):
        super(FeatureNet, self).__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)
        self.feature = nn.Linear(12288, FeatureNet.num_features)
        self.sig = nn.Sigmoid()

    def forward(self, x):
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)

        x = self.resnet.layer1(x)
        x = self.resnet.layer2(x)
        x = self.resnet.layer3(x)
        x = self.resnet.layer4(x)

        x = self.resnet.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.feature(x)

        return self.sig(x)

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)


class PhaseNet(nn.Module):
    """PhaseNet."""

    lstm_size = 512
    """Number of LSTM neurons."""

    # featureNet: pretrained featureNet (.pkl file) that is to be used
    def __init__(self, featureNet=None):
        """Create PhaseNet.

        :param featureNet: Path to a file that stores the model parameters of a FeatureNet.
                           If specified, the FeatureNet layers will be initialized with weights read from this file.
        """
        super(PhaseNet, self).__init__()
        self.featureNet = FeatureNet()

        if featureNet is not None:
            self.featureNet.load(featureNet)

        self.lstm = nn.LSTM(FeatureNet.num_features, PhaseNet.lstm_size, batch_first=True)
        self.classifier = nn.Linear(PhaseNet.lstm_size, Cholec80.num_phases)

    def init_hidden(self, device=None):
        return (torch.zeros(1, 1, PhaseNet.lstm_size, device=device),
                torch.zeros(1, 1, PhaseNet.lstm_size, device=device))

    def forward(self, x, hidden_state):
        x = self.featureNet.forward(x)
        x = x.view(1, x.size(0), -1)
        x, hidden_state = self.lstm(x, hidden_state)
        x = x.view(x.size(1), -1)
        x = self.classifier(x)

        return x, hidden_state

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)
