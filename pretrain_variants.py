import torch
import torch.nn as nn
import torch.nn.functional as F


def step_contrastive(data, margin, net, optimizer, device, train=True):
    """Process a batch during self-supervised pretraining using contrastive loss.

    :param data: The batch to process.
    :param margin: The margin parameter to use (float).
    :param net: The FeatureNet to use.
    :param optimizer: The torch.optim.optimizer.Optimizer to use.
    :param device: The torch.Device to use.
    :param train: Whether or not the batch is to be processed in training mode, i.e., making an optimization step.
    :return: loss, dist_close, dist_far
             loss: The mean loss averaged over all tuples in the batch.
             dist_close: Distance between close frame and reference frame. Numpy.ndarray with one entry per tuple in the batch.
             dist_far: Distance between distant frame and reference frame. Numpy.ndarray with one entry per tuple in the batch.
    """
    img, img_c, img_f = data
    stream_img = img.to(device)
    stream_c = img_c.to(device)
    stream_f = img_f.to(device)

    if train:
        optimizer.zero_grad()

    embedding_img = net(stream_img)
    embedding_c = net(stream_c)
    embedding_f = net(stream_f)

    dist_close = F.pairwise_distance(embedding_img, embedding_c, p=2)
    dist_far = F.pairwise_distance(embedding_img, embedding_f, p=2)
    dist = torch.cat((dist_close, dist_far), 0)

    # we additionally square both summands in this implementation
    loss_contrastive = torch.mean(torch.pow(dist_close, 2) + torch.pow(torch.clamp(margin - dist_far, min=0.0), 2))

    if train:
        loss_contrastive.backward()
        optimizer.step()

    D = dist.data.cpu().numpy().squeeze()
    return loss_contrastive.item(), D[:len(img)], D[len(img):]


def step_contrastive_1stAnd2nd(data, margin, omega, net, optimizer, device, train=True):
    """Process a batch during self-supervised pretraining using 1st and 2nd order contrastive loss.

    :param data: The batch to process.
    :param margin: The margin parameter to use (float).
    :param omega: The weight parameter used to balance 1st and 2nd order temporal coherency (float).
    :param net: The FeatureNet to use.
    :param optimizer: The torch.optim.optimizer.Optimizer to use.
    :param device: The torch.Device to use.
    :param train: Whether or not the batch is to be processed in training mode, i.e., making an optimization step.
    :return: loss, dist_close, dist_far
             loss: The mean loss averaged over all tuples in the batch.
             dist_close: Distance between close frame and reference frame. Numpy.ndarray with one entry per tuple in the batch.
             dist_far: Distance between distant frame and reference frame. Numpy.ndarray with one entry per tuple in the batch.
    """
    img, img_c, img_c2, img_f = data
    stream_img = img.to(device)
    stream_c = img_c.to(device)
    stream_c2 = img_c2.to(device)
    stream_f = img_f.to(device)

    if train:
        optimizer.zero_grad()

    embedding_img = net(stream_img)
    embedding_c = net(stream_c)
    embedding_c2 = net(stream_c2)
    embedding_f = net(stream_f)

    dist_close = F.pairwise_distance(embedding_img, embedding_c, p=2)
    dist_far = F.pairwise_distance(embedding_img, embedding_f, p=2)
    dist = torch.cat((dist_close, dist_far), 0)

    loss_contrastive_1st = torch.mean(torch.pow(dist_close, 2) + torch.pow(torch.clamp(margin - dist_far, min=0.0), 2))

    diff_img_c = embedding_img - embedding_c
    diff_c_c2 = embedding_c - embedding_c2
    diff_c_f = embedding_c - embedding_f

    dist_close_2nd = F.pairwise_distance(diff_img_c, diff_c_c2, p=2)
    dist_far_2nd = F.pairwise_distance(diff_img_c, diff_c_f, p=2)

    loss_contrastive_2nd = torch.mean(torch.pow(dist_close_2nd, 2) + torch.pow(torch.clamp(margin - dist_far_2nd, min=0.0), 2))
    loss_contrastive_1stAnd2nd = loss_contrastive_1st + omega * loss_contrastive_2nd

    if train:
        loss_contrastive_1stAnd2nd.backward()
        optimizer.step()

    D = dist.data.cpu().numpy().squeeze()
    return loss_contrastive_1stAnd2nd.item(), D[:len(img)], D[len(img):]


def step_ranking(data, margin, net, optimizer, device, train=True):
    """Process a batch during self-supervised pretraining using ranking loss.

    :param data: The batch to process.
    :param margin: The margin parameter to use (float).
    :param net: The FeatureNet to use.
    :param optimizer: The torch.optim.optimizer.Optimizer to use.
    :param device: The torch.Device to use.
    :param train: Whether or not the batch is to be processed in training mode, i.e., making an optimization step.
    :return: loss, dist_close, dist_far
             loss: The mean loss averaged over all tuples in the batch.
             dist_close: Distance between close frame and reference frame. Numpy.ndarray with one entry per tuple in the batch.
             dist_far: Distance between distant frame and reference frame. Numpy.ndarray with one entry per tuple in the batch.
    """
    criterion = nn.MarginRankingLoss(margin=margin, size_average=True)

    img, img_c, img_f = data
    stream_img = img.to(device)
    stream_c = img_c.to(device)
    stream_f = img_f.to(device)
    target = torch.FloatTensor(len(img), 1).fill_(-1).to(device)

    if train:
        optimizer.zero_grad()

    embedding_img = net(stream_img)
    embedding_c = net(stream_c)
    embedding_f = net(stream_f)

    dist_close = F.pairwise_distance(embedding_img, embedding_c, p=2)
    dist_far = F.pairwise_distance(embedding_img, embedding_f, p=2)
    dist = torch.cat((dist_close, dist_far), 0)

    loss_ranking = criterion(dist_close, dist_far, target)

    if train:
        loss_ranking.backward()
        optimizer.step()

    D = dist.data.cpu().numpy().squeeze()
    return loss_ranking.item(), D[:len(img)], D[len(img):]
