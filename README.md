# Temporal coherence-based self-supervised learning for laparoscopic workflow analysis

## Background
In the field of Computer Assisted Surgery (CAS), **surgical phase segmentation** is a prominent problem. It is the task to automatically recognize which **surgical phase** is being performed by the surgeon at each point during surgery,  given some sensory input (e.g., RFID, device data, audio, video). Solving this task is an important step towards context-aware surgical assistance. 

In minimally-invasive surgery, the straightforward way to acquire data for surgical phase segmentation is to use the **video frames** coming from the endoscopic/ laparoscopic camera. Recently, deep learning approaches (esp. CNN-LSTM architectures) have shown promising results for video-based surgical phase segmentation. However, these methods require large amounts of training data, i.e., annotated surgical videos, which is expensive and difficult to obtain. Thus, methods to learn from **unlabeled** surgical videos are of special interest.

## Contribution
We propose to learn from unlabeled laparoscopic videos in a **self-supervised** manner. More specifically, we use a **temporal coherence**-based proxy task to **pretrain** a CNN on unlabeled laparoscopic videos. The complete CNN-LSTM network can then be finetuned for surgical phase segmentation using the available labeled data.

The temporal coherence-based proxy task is inspired by the observation that video frames change slowly and steadily over time. Hence, we train the CNN to learn an embedding that maps temporally close video frames to similar representations in feature space. This idea goes back to Slow Feature Analysis.

Details can be found in our [paper](https://arxiv.org/abs/1806.06811).

## Code

### Requirements
The implementation is in Python 3.5 using [PyTorch 0.4.0](https://pytorch.org).
We use the following Python packages:
> torch torchvision numpy os sys random argparse time datetime csv imageio PIL skimage

### Run the code

#### Before you start
Please specify where your data is by setting the `data_path` variable in `utils.py` to the correct file path (see [Get the data](#get-the-data) for information on how to get the data we used for our experiments). Please also set the `out_path` variable in `utils.py` to the file path to the directory where results from the experiments are to be stored.

For each experiment that you run, the results will be written to a distinct subfolder of `out_path`. When [pretraining](#pretrain-your-models) a CNN, results such as the learned model parameters will be stored in `<out_path>/Self-supervised/<variant>/<trial_id>`. When [finetuning](#finetune-for-surgical-phase-segmentation) a CNN-LSTM for surgical phase segmentation, results such as model parameters, predictions, and evaluation scores will be stored in `<out_path>/Phase_segmentation/<variant>/<#OPs>/<trial_id>`. Here, `<variant>` denotes the self-supervised pretraining variant that is employed, namely `contrastive`, `ranking`, or `1st+2nd_contrastive`. In the case of finetuning, this can also be `no_pretrain`. `<trial_id>` is meant to be a unique identifier for each experiment that you run. So far, it is calculated based on the date and time of the moment when you start the experiment (e.g. `20180601-0933`). In the case of finetuning, `<#OPs>` denotes how many labeled OP videos are used, namely `20`, `40`, or `60`.

#### Get the data
Download Cholec80 from [here](http://camma.u-strasbg.fr/datasets) and unzip it: `unzip cholec80.zip -d cholec80`  
The folder that contains all of the Cholec80 data afterwards should be called  `cholec80`.  Set `data_path` (in `utils.py`) to the path to this folder,  e.g., `"/media/data/cholec80"`.

We prepared the data by 
- splitting the videos into the four sets A, B, C, and D
- extracting the video frames at 5 Hz from each video and downsizing them to 384 x 216 px

You can do the same by running `python3 prepare_dataset.py`, which will create the `frames` folder in your `cholec80` folder. Beware: this will take some time and require some memory (106 GB for the `frames` folder!).

Afterwards, we assume that your data has the following structure:
```
cholec80
	frames
		A
			02
				00000000.png
				00000001.png
				...
			04
			...
		B
			01
			...
		C
		D	
	phase_annotations
		video01-phase.txt
		...
	videos
		video01.mp4
		...
```
For every OP 01, 02, ..., 80 you have a corresponding folder 01, 02, ..., 80 in one of the folders A, B, C, or D (the set to which the OP video belongs). Each folder 01, 02, ..., 80 contains the corresponding frames, numbered consecutively using 8-digit integers. 

#### Pretrain a model
You can pretrain a FeatureNet using the proposed temporal coherence-based approach by running `python3 pretrain_self_supervised.py <variant>`.
Instead of `<variant>`, please type either `contrastive`, `ranking`, or `1st+2nd_contrastive`, depending on which variant you want to use.
The pretrained model will be saved to `<out_path>/Self-supervised/<variant>/<trial_id>/model.pth`.  
There is a number of parameters that you can modify using the command line. Run `python3 pretrain_self_supervised.py -h` in order to get a list of all command line options.

Examples:  
`python3 pretrain_self_supervised.py contrastive`  
`python3 pretrain_self_supervised.py ranking --delta 2.2 --epochs 99`

#### Finetune for surgical phase segmentation
You can finetune a PhaseNet for surgical phase segmentation by running `python3 train_phase_segm.py <#OPs> [-m <pretrained_model>]`.
Instead of `<#OPs>`, please type either `20`, `40`, or `60`, depending on how many labeled OP videos should be used for training. If you want to re-use the weights of a pretrained FeatureNet, you can specify the pretrained model using the command line option `-m`. It is sufficient to specify `<variant>/<trial_id>`, e.g. `contrastive/20180601-0933`. The script will attempt to locate the file `<out_path>/Self-supervised/<variant>/<trial_id>/model.pth` and to read the pretrained weights from there. Alternatively, you can  specify the complete file path to the file storing the pretrained model.
The finetuned PhaseNet will be saved to `<out_path>/Phase_segmentation/<variant>/<#OPs>/<trial_id>/model.pkl`.  
There is a number of parameters that you can modify using the command line. Run `python3 train_phase_segm.py -h` in order to get a list of all command line options.

Examples:  
`python3 train_phase_segm.py 20`  
`python3 train_phase_segm.py 20 -m contrastive/20180601-0933`  
`python3 train_phase_segm.py 20 -m contrastive/20180601-0933 --epochs 99`  
Note that you need to specify the correct trial id, i.e., the id of an experiment that you have actually run. 

#### Evaluate
To calculate evaluation metrics for surgical phase segmentation, you can run `python3 eval.py <variant>/<num_ops>/<trial_id>`. Here, `<variant>/<num_ops>/<trial_id>` specifies the experiment that you want to evaluate.
Running the script will print 
- Mean and standard deviation of accuracy, avg. recall, avg. precision, and avg. F1 score, calculated over all test OP videos
- Mean and standard deviation of the F1 score per phase, calculated over all test OP videos

Note that the script requires the folder `<out_path>/Phase_segmentation/<variant>/<num_ops>/<trial_id>/predictions` to be present. This folder is created automatically when `train_phase_segm.py` is being executed.

Examples:  
`python3 eval.py no_pretrain/60/20180603-0800`  
`python3 eval.py contrastive/20/20180603-1915`  
Note that you need to specify the correct trial id, i.e., the id of an experiment that you have actually run. 

You can also reproduce our **image retrieval** experiment by running `python3 image_retrieval.py <variant>/<trial_id>`. Here, `<variant>/<trial_id>` specifies the experiment, i.e., the pretrained model, that you want to evaluate.  
For each query frame (in the default setting: frames 00000290, 00002520, 00007445, and 00007625 from OP video 01), this will retrieve, from each test OP video, the frame that is closest in feature space. This is the frame whose embedding, as learned by the model we evaluate, is closest to the embedding of the query frame (measured by Euclidean distance).  
For each query frame, a folder will be created in `<out_path>/Image_retrieval` (e.g. `out_path/Image_retrieval/01_00000290`) and the retrieved frames will be stored to that folder. The file names of the retrieved frames will start with the distance to the query frame (e.g. `0.16_28_00000840.png`).

To calculate embeddings, the FeatureNet saved at `<out_path>/Self-supervised/<variant>/<trial_id>/model.pth` will be used. Embeddings will be calculated on demand and will be cached at `out_path/Self-supervised/<variant>/<trial_id>/embeddings`.

You can specify custom query frames using the command line options
- `-f` To specify one or more query frames, using their 8-digit integer identifiers. Each identifier should be a multiple of 5.
- `-o` To specify the OP video to which the query frames belong.
- `-s` To specify the OP video set to which the OP video (specified using option `-o`) belongs.

For example, `-f 00004444 00001010 -o 06 -s A` specifies two query frames, stored at `<data_path>/frames/A/06/00004444.png` and `<data_path>/frames/A/06/00001010.png` respectively.

Examples:  
`python3 image_retrieval.py 1st+2nd_contrastive/20180601-1830`  
`python3 image_retrieval.py 1st+2nd_contrastive/20180601-1830 -f 00002000 00004000 00006000 -o 17 -s C`  
Note that you need to specify the correct trial id, i.e., the id of an experiment that you have actually run. 

### How to cite
If you use parts of the code in your own research, please cite: 

    @InProceedings{funke2018temporal,
      title="Temporal Coherence-based Self-supervised Learning for Laparoscopic Workflow Analysis",
      author="Funke, Isabel and Jenke, Alexander and Mees, S{\"o}ren Torge and Weitz, J{\"u}rgen and Speidel, Stefanie and Bodenstedt, Sebastian",
      editor="Stoyanov, Danail and Taylor, Zeike and Sarikaya, Duygu and McLeod, Jonathan and Gonz{\'a}lez Ballester, Miguel Angel and Codella, Noel C.F. and Martel, Anne and Maier-Hein, Lena and Malpani, Anand and Zenati, Marco A. and De Ribaupierre, Sandrine and Xiongbiao, Luo and Collins, Toby and Reichl, Tobias and Drechsler, Klaus and Erdt, Marius and Linguraru, Marius George and Oyarzun Laura, Cristina and Shekhar, Raj and Wesarg, Stefan and Celebi, M. Emre and Dana, Kristin and Halpern, Allan",
      booktitle="OR 2.0 Context-Aware Operating Theaters, Computer Assisted Robotic Endoscopy, Clinical Image-Based Procedures, and Skin Image Analysis",
      year="2018",
      pages="85--93",
      series="Lecture Notes in Computer Science",
      volume="11041",
      publisher="Springer International Publishing",
      address="Cham",
      doi="10.1007/978-3-030-01201-4_11"
    }  
    
The paper was presented at the Workshop on Context-Aware Operating Theaters ([OR 2.0](https://or20.univ-rennes1.fr/)), a [MICCAI](https://www.miccai2018.org/en/) satellite event. 

### Thanks to the people who contributed to the code!
- Sebastian Bodenstedt
- Isabel Funke
- Alexander Jenke

(listed alphabetically)

This work was carried out at the National Center for Tumor Diseases (NCT) Dresden, [Department of Translational Surgical Oncology](https://www.nct-dresden.de/tso.html)
